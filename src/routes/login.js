const express = require('express')
const router = express.Router()

const users = require("../controllers/users")

module.exports = (app, res) => {
    app.use("/api", router)
    router.get("/signin", users.signin)
}